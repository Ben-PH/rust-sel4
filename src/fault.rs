// Copyright (c) 2017 The Robigalia Project Developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.

use sel4_sys::{seL4_DebugException, seL4_VMFault, seL4_UnknownSyscall, seL4_UserException,
               seL4_FaultType, seL4_UnknownSyscallReply, seL4_UserExceptionReply,
               seL4_DebugExceptionReply, seL4_CapFault, seL4_GetIPCBuffer, seL4_GuardMismatch};

use {Endpoint, LookupFailureKind, Reply, RecvToken, try_reply_then_recv};

/// Receive, interpret, and reply to fault messages
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum FaultMsg {
    UnknownFault,
    CapFault(CapFaultToken),
    VmFault(VmFaultToken),
    UnknownSyscall(UnknownSyscallToken),
    UserException(UserExceptionToken),
    DebugException(DebugExceptionToken),
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct CapFaultToken {
    pub msg: seL4_CapFault,
    pub lookup_failure_kind: Option<LookupFailureKind>,
}

impl CapFaultToken {
    fn from_ipcbuf() -> CapFaultToken {
        let (msg, index) = unsafe { seL4_CapFault::from_ipcbuf(0) };
        let lookup_failure_kind;
        if msg.lookup_failure_type <= seL4_GuardMismatch as usize {
            lookup_failure_kind = unsafe { LookupFailureKind::from_ipcbuf(seL4_GetIPCBuffer(),
                                                                 msg.lookup_failure_type,
                                                                 index as usize) };
        } else {
            lookup_failure_kind = None;
        }
        CapFaultToken {
            msg: msg,
            lookup_failure_kind: lookup_failure_kind,
        }
    }

    /// Restart the faulted thread by replying to the message. Does not block.
    pub fn resolve_fault(&self, reply: Reply) {
        reply_to_restart(reply);
    }

    /// Restart the faulted thread by repling to the message. Then, block waiting
    /// for another FaultMsg.
    pub fn resolve_fault_then_recv(&self, reply: Reply, ep: Endpoint) -> (FaultMsg, RecvToken) {
        reply_to_restart_then_recv(reply, ep)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct VmFaultToken {
    pub msg: seL4_VMFault,
}

impl VmFaultToken {
    fn from_ipcbuf() -> VmFaultToken {
        VmFaultToken {
            msg: unsafe { seL4_VMFault::from_ipcbuf(0).0 },
        }
    }

    /// Restart the faulted thread by replying to the message. Does not block.
    pub fn resolve_fault(&self, reply: Reply) {
        reply_to_restart(reply);
    }

    /// Restart the faulted thread by repling to the message. Then, block waiting
    /// for another FaultMsg.
    pub fn resolve_fault_then_recv(&self, reply: Reply, ep: Endpoint) -> (FaultMsg, RecvToken) {
        reply_to_restart_then_recv(reply, ep)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct UnknownSyscallToken {
    pub msg: seL4_UnknownSyscall,
}

impl UnknownSyscallToken {
    fn from_ipcbuf() -> UnknownSyscallToken {
        UnknownSyscallToken {
            msg: unsafe { seL4_UnknownSyscall::from_ipcbuf(0).0 },
        }
    }

    /// Restart the faulted thread by replying to the message. Does not block.
    ///
    /// `message` optionally Sets the faulting thread's registers before restarting.
    pub fn resolve_fault(&self, reply: Reply, message: Option<seL4_UnknownSyscallReply>) {
        reply_message_to_restart(reply, message);
    }

    /// Restart the faulted thread by replying to the message. Then, block waiting
    /// for another FaultMsg.
    ///
    /// `message` optionally sets the faulting thread's registers before restarting.
    pub fn resolve_fault_then_recv(&self, reply: Reply, message: Option<seL4_UnknownSyscallReply>, ep: Endpoint) -> (FaultMsg, RecvToken) {
        reply_message_to_restart_then_recv(reply, message, ep)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct UserExceptionToken {
    pub msg: seL4_UserException,
}

impl UserExceptionToken {
    fn from_ipcbuf() -> UserExceptionToken {
        UserExceptionToken {
            msg: unsafe { seL4_UserException::from_ipcbuf(0).0 },
        }
    }

    /// Restart the faulted thread by replying to the message. Does not block.
    ///
    /// `message` optionally sets the faulting thread's instruction pointer, stack pointer,
    /// and flags register before restarting.
    pub fn resolve_fault(&self, reply: Reply, message: Option<seL4_UserExceptionReply>) {
        reply_message_to_restart(reply, message);
    }

    /// Restart the faulted thread by replying to the message. Then, block waiting
    /// for another FaultMsg.
    ///
    /// `message` optionally sets the faulting thread's instruction pointer, stack pointer,
    /// and flags register before restarting.
    pub fn resolve_fault_then_recv(&self, reply: Reply, message: Option<seL4_UserExceptionReply>, ep: Endpoint) -> (FaultMsg, RecvToken) {
        reply_message_to_restart_then_recv(reply, message, ep)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct DebugExceptionToken {
    pub msg: seL4_DebugException,
}

impl DebugExceptionToken {
    fn from_ipcbuf() -> DebugExceptionToken {
        DebugExceptionToken {
            msg: unsafe { seL4_DebugException::from_ipcbuf(0).0 },
        }
    }

    /// Restart the faulted thread by replying to the message. Does not block.
    ///
    /// `message` optionally sets the number of instructions to skip before restarting the
    /// faulting thread. This is only meaningful when single stepping.
    pub fn resolve_fault(&self, reply: Reply, message: Option<seL4_DebugExceptionReply>) {
        reply_message_to_restart(reply, message);
    }

    /// Restart the faulted thread by replying to the message. Then, block waiting
    /// for another FaultMsg.
    ///
    /// `message` optionally sets the number of instructions to skip before restarting the
    /// faulting thread. This is only meaningful when single stepping.
    pub fn resolve_fault_then_recv(&self, reply: Reply, message: Option<seL4_DebugExceptionReply>, ep: Endpoint) -> (FaultMsg, RecvToken) {
        reply_message_to_restart_then_recv(reply, message, ep)
    }
}

impl FaultMsg {
    /// Process a received message as a fault message
    pub fn from_recv_token(msg: &RecvToken) -> FaultMsg {
        if msg.label == seL4_FaultType::CapFault as usize {
            FaultMsg::CapFault(CapFaultToken::from_ipcbuf())
        } else if msg.label == seL4_FaultType::VMFault as usize {
            FaultMsg::VmFault(VmFaultToken::from_ipcbuf())
        } else if msg.label == seL4_FaultType::UnknownSyscall as usize {
            FaultMsg::UnknownSyscall(UnknownSyscallToken::from_ipcbuf())
        } else if msg.label == seL4_FaultType::UserException as usize {
            FaultMsg::UserException(UserExceptionToken::from_ipcbuf())
        } else if msg.label == seL4_FaultType::DebugException as usize {
            FaultMsg::DebugException(DebugExceptionToken::from_ipcbuf())
        } else {
            FaultMsg::UnknownFault
        }
    }

    /// Receive a message on an endpoint and process it as a fault message
    pub fn recv(ep: Endpoint, reply: Reply) -> (FaultMsg, RecvToken) {
        let recv_token = ep.recv(reply);
        (FaultMsg::from_recv_token(&recv_token), recv_token)
    }
}

trait FaultReply {
    unsafe fn to_ipcbuf(&self) -> isize;
}

impl FaultReply for seL4_UnknownSyscallReply {
    unsafe fn to_ipcbuf(&self) -> isize {
        self.to_ipcbuf(0)
    }
}

impl FaultReply for seL4_UserExceptionReply {
    unsafe fn to_ipcbuf(&self) -> isize {
        self.to_ipcbuf(0)
    }
}

impl FaultReply for seL4_DebugExceptionReply {
    unsafe fn to_ipcbuf(&self) -> isize {
        self.to_ipcbuf(0)
    }
}

fn reply_to_restart(reply: Reply) {
    reply.try_send(0, 0, 0);
}

fn reply_message_to_restart<T: FaultReply>(reply: Reply, message: Option<T>) {
    let len;
    match message {
        Some(m) => len = unsafe { m.to_ipcbuf() as usize },
        _ => len = 0,
    }
    reply.try_send(0, len, 0);
}

fn reply_to_restart_then_recv(reply: Reply, ep: Endpoint) -> (FaultMsg, RecvToken) {
    let recv_token = try_reply_then_recv(reply, 0, 0, 0, ep);
    (FaultMsg::from_recv_token(&recv_token), recv_token)
}

fn reply_message_to_restart_then_recv<T: FaultReply>(reply: Reply, message: Option<T>, ep: Endpoint) -> (FaultMsg, RecvToken) {
    let len;
    match message {
        Some(m) => len = unsafe { m.to_ipcbuf() as usize },
        _ => len = 0,
    }
    let recv_token = try_reply_then_recv(reply, 0, len, 0, ep);
    (FaultMsg::from_recv_token(&recv_token), recv_token)
}
